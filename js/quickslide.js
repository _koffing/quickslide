'use strict';

function QuickSlide (slider, options){
  this._slider = document.querySelector(slider)
  this._slidesContainer = this._slider.children[0];
  this._slides = this._slidesContainer.children;
  this._indicatorItems = [];
  this._slideData = [];
  this._activeSlide = null;
  this._nextSlide;
  this._prevSlide;
  this._arrowLeft;
  this._arrowRight;
  this._arrowsActive = true;
  this._indicatorsActive = true;
  this._activeSlideCheck = true;
  this.transition = 300;
  return this.init(slider, options);
} 

QuickSlide.prototype.init = function(slider, options){
  this.setOptions(options);
  this.setStyles();
   //run methods that create elements first.
  if (this._slides.length != 1){
       this.setArrows(); // creates arrowes.
       this.setIndicators(); // creates indicator list.
       this.setSlideData();
       this.setTiming();
       this.swipeEvents(slider);
       this.dragEvents(slider);
       this.arrowEvents();
       this.indicatorEvents();
       this.throttledResize();
   }
}

QuickSlide.prototype.setOptions = function(options){
    if(options){
        if(options.transition != undefined){
            this.transition = options.transition;
        }
        if(options.arrows != undefined && options.arrows != true){
            this._arrowsActive = false;
        }
        if(options.indicators  != undefined && options.indicators != true){
            this._indicatorsActive = false;
        }
        if (options.activeSlideCheck != undefined && options.activeSlideCheck != true) {
            this._activeSlideCheck = false;
        }

    }
}

QuickSlide.prototype.setSlideData = function(){
  var self = this
  var i = 0;
  for(i; i < self._slides.length; i++){
      var data = {
          el: self._slides[i],
          index: i,
      }
    self._slideData.push(data);
  }
}

QuickSlide.prototype.setTiming = function(timing){
    var self = this;
    if(timing != undefined){
        self._slidesContainer.style.transitionDuration = (timing/1000 + "s");
    }else {
        setTimeout(function() {
            self._slidesContainer.style.transitionDuration = (self.transition/1000 + "s");
        }, 50);
    }
}


QuickSlide.prototype.setSliderHeight = function(){
    var self = this;
    var heightSet = 0;
    var comparisonHeight;
    var i;  
      for(i = 0; i < self._slides.length; i++){
          comparisonHeight = self._slides[i].clientHeight;
          if(comparisonHeight > heightSet) {
              heightSet = comparisonHeight;
          }
      }
  
      self._slidesContainer.style.height = heightSet + "px"; 
  }

/*
    add the ability to check for an active class and set slide classes accordingly 
*/
QuickSlide.prototype.setStyles = function(){
  var self = this;
  var i;  

    self.setSliderHeight();
  
    for(i = 0; i < self._slides.length; i++){
        self._slides[i].id = "slide-"+i
        
        if (self._activeSlideCheck){
            if (self.hasClass(self._slides[i], "active")) {
                if (i == 0) {
                    self._activeSlide = i;
                    self._nextSlide = i + 1;
                    self._prevSlide = self._slides.length - 1;
                } else if (i == (self._slides.length - 1)) {
                    self._activeSlide = i;
                    self._nextSlide = 0;
                    self._prevSlide = i - 1;
                } else {
                    self._activeSlide = i;
                    self._nextSlide = i + 1;
                    self._prevSlide = i - 1;
                }
            }
        }
    }
  if (self._activeSlide == null){
      self._activeSlide = 0;
      self._nextSlide = 1;
      self._prevSlide = self._slides.length - 1;
  }
  if (self._slides.length == 1) {
      self.addClass(self._slides[self._activeSlide], "active");
      self._slides[self._activeSlide].style.opacity = "1";
  } else if(self._slides.length == 2){
      if (self._activeSlide == 0){
          self.addClass(self._slides[self._activeSlide], "active");
          self._slides[self._activeSlide].style.opacity = "1";
          self._slides[self._prevSlide].style.opacity = "1";
          self._slides[self._prevSlide].style.display = "none";
      }else {
          self.addClass(self._slides[self._activeSlide], "active");
          self._slides[self._activeSlide].style.opacity = "1";
          self._slides[self._prevSlide].style.opacity = "1";
          self._slides[self._prevSlide].style.display = "none";
      }
  } else {
      self.addClass(self._slides[self._activeSlide], "active");
      self._slides[self._activeSlide].style.opacity = "1";
      self.addClass(self._slides[self._prevSlide], "prev");
      self._slides[self._prevSlide].style.opacity = "0";
      self.addClass(self._slides[self._nextSlide], "next");
      self._slides[self._nextSlide].style.opacity = "0";
  }
    self.addClass(this._slider, 'styles-set');
}

QuickSlide.prototype.setArrows = function(){
    if(this._arrowsActive){
        this._arrowLeft = document.createElement('div');
        this._arrowRight = document.createElement('div');
        this._arrowLeft.className = 'slide-arrow slide-arrow-left'
        this._arrowRight.className = 'slide-arrow slide-arrow-right'
        this._slider.appendChild(this._arrowLeft);
        this._slider.appendChild(this._arrowRight);
        this.addClass(this._slider, 'active-arrows');
    }
}

QuickSlide.prototype.setIndicators = function(){
    if(this._indicatorsActive){
        var self = this;
        this.slideIndicators = document.createElement('ul');
        this.slideIndicators.className = 'slide-indicators';
        var slideIndicatorDot;
        var i = 0;
        for(i; i < self._slides.length; i++){
            slideIndicatorDot = document.createElement('li');
            slideIndicatorDot.setAttribute('data-index', i);
            if(i == this._activeSlide){
                slideIndicatorDot.className = 'active';
            }
            this._indicatorItems.push(slideIndicatorDot);
            this.slideIndicators.appendChild(slideIndicatorDot);
        }
        this._slider.appendChild(this.slideIndicators);
        self.addClass(self._slider, 'active-indicators');
    }
}

QuickSlide.prototype.shiftSlideIndex = function(modifier){
    var self = this;
    if(self._slides.length == 2){
        if (modifier == 'left') {
            if (this._activeSlide == 0) {
                this._activeSlide = 1;
                this._prevSlide = 0;
                this._nextSlide = 0;
            } else {
                this._activeSlide = 0;
                this._prevSlide = 1;
                this._nextSlide = 1;
            }
        } else if (modifier == 'right') {
            if (this._activeSlide == 0) {
                this._activeSlide = 1;
                this._prevSlide = 0;
                this._nextSlide = 0;
            } else {
                this._activeSlide = 0;
                this._prevSlide = 1;
                this._nextSlide = 1;
            }
        }

    } else {
        if(modifier == 'left'){
            if(this._activeSlide == 0){
                this._activeSlide ++;
                this._prevSlide = 0;
                this._nextSlide = this._activeSlide + 1;
            } else if(this._activeSlide == this._slides.length - 1){
                this._activeSlide = 0;
                this._prevSlide = this._slides.length - 1;
                this._nextSlide = this._activeSlide + 1;
            } else {
                this._activeSlide ++;
                this._prevSlide = this._activeSlide - 1;
                if(this._activeSlide == this._slides.length - 1){
                    this._nextSlide = 0;
                }else {
                    this._nextSlide = this._activeSlide + 1;
                }
            }
        }else if(modifier == 'right') {
            if(this._activeSlide == 0){
                this._activeSlide = this._slides.length - 1;
                this._prevSlide = this._activeSlide - 1;
                this._nextSlide = 0;
            } else if(this._activeSlide == this._slides.length - 1){
                this._activeSlide --;
                this._prevSlide = this._activeSlide - 1;
                this._nextSlide = this._activeSlide + 1;
            } else {
                this._activeSlide --;
                this._nextSlide = this._activeSlide + 1;
                if(this._activeSlide == 0){
                    this._prevSlide = this._slides.length - 1;
                }else {
                    this._prevSlide = this._activeSlide - 1;
                }
            }
        }
    }
}


QuickSlide.prototype.shiftSlides = function(direction){
    var self = this;
    // check if slider is currently animating to prevent more slide animations.
    if(!self.hasClass(self._slidesContainer, 'sliding')){

        self.addClass(self._slidesContainer, 'sliding');
        setTimeout(function(){
            self.removeClass(self._slidesContainer, 'sliding');
        }, self.transition + 50);

        //store previous active states.
        var lastClassState = {
            lastActive: self._activeSlide,
            lastPrev: self._prevSlide,
            lastNext: self._nextSlide
        }

        self.shiftSlideIndex(direction);
        //if slider has only 2 slides
        if(self._slides.length == 2){
            if (direction == 'left') {
                self.removeClass(self._slideData[lastClassState.lastPrev].el, 'prev');
                self.addClass(self._slideData[lastClassState.lastNext].el, 'next');
                self._slideData[lastClassState.lastNext].el.style.display = 'block';
                setTimeout(function () {
                    self.addClass(self._slideData[self._prevSlide].el, 'prev'); //active
                    self.addClass(self._slideData[self._activeSlide].el, 'active');
                }, 60);

                setTimeout(function () {
                    self.removeClass(self._slideData[lastClassState.lastActive].el, 'active'); //active
                    self.removeClass(self._slideData[lastClassState.lastNext].el, 'next');
                    self.removeClass(self._slideData[lastClassState.lastPrev].el, 'prev');
                }, 85);
                setTimeout(function () {
                    self._slideData[lastClassState.lastActive].el.style.display = 'none';
                }, self.transition + 70);
            } else if (direction == 'right') {
                self.removeClass(self._slideData[lastClassState.lastNext].el, 'next');
                self.addClass(self._slideData[lastClassState.lastPrev].el, 'prev');
                self._slideData[lastClassState.lastPrev].el.style.display = 'block';
                setTimeout(function () {
                    self.addClass(self._slideData[self._nextSlide].el, 'next'); //active
                    self.addClass(self._slideData[self._activeSlide].el, 'active');
                }, 60);

                setTimeout(function () {
                    self.removeClass(self._slideData[lastClassState.lastActive].el, 'active'); //active
                    self.removeClass(self._slideData[lastClassState.lastNext].el, 'next');
                    self.removeClass(self._slideData[lastClassState.lastPrev].el, 'prev');
                }, 85);
                setTimeout(function () {
                    self._slideData[lastClassState.lastActive].el.style.display = 'none';
                }, self.transition + 70);
            }
         //if slider has more than 2 slides
        } else {
            //set opacity so slides can not be seen 
            if (direction == 'left') {
                self._slideData[lastClassState.lastPrev].el.style.opacity = "0";
                self._slideData[lastClassState.lastNext].el.style.opacity = "1";
                setTimeout(function () {
                    self._slideData[lastClassState.lastActive].el.style.opacity = "0";
                }, self.transition);
            } else if (direction == 'right') {
                self._slideData[lastClassState.lastPrev].el.style.opacity = "1";
                self._slideData[lastClassState.lastNext].el.style.opacity = "0";
                setTimeout(function () {
                    self._slideData[lastClassState.lastActive].el.style.opacity = "0";
                }, self.transition);
            }
            self.addClass(self._slideData[self._prevSlide].el, 'prev'); 
            self.addClass(self._slideData[self._activeSlide].el, 'active');
            self.addClass(self._slideData[self._nextSlide].el, 'next');
            self.removeClass(self._slideData[lastClassState.lastActive].el, 'active'); 
            self.removeClass(self._slideData[lastClassState.lastNext].el, 'next');
            self.removeClass(self._slideData[lastClassState.lastPrev].el, 'prev');
        }
        
    } else {
        return;
    }

    //set the new active indicator
    if(self._indicatorsActive){
        self.removeClass(self._indicatorItems[lastClassState.lastActive], 'active');
        self.addClass(self._indicatorItems[self._activeSlide], 'active');
    }

}

 
QuickSlide.prototype.swipeEvents = function(slider){
    var self = this;
  this.swipeLeft(slider, function(){
      self.shiftSlides('left');
  });
   this.swipeRight(slider, function(){
      self.shiftSlides('right');
  });
}

QuickSlide.prototype.dragEvents = function () {
    var self = this;
    var startX = null;

    setSlideDragEvent();
    function setSlideDragEvent(){
        self._slider.addEventListener("mousedown", function(e){
            this.addEventListener('mousemove', mouseMoveEvent);
        });
        self._slider.addEventListener("mouseup", function (e) {
            this.removeEventListener('mousemove', mouseMoveEvent);
        });
        function mouseMoveEvent(e){
            if (startX == null){
                startX = e.clientX;
            }else {
                if ((startX - e.clientX) > 50) {
                    self.shiftSlides('left');
                    startX = null;
                    this.removeEventListener('mousemove', mouseMoveEvent);
                } else if ((startX - e.clientX) < -50){
                    self.shiftSlides('right');
                    startX = null;
                    this.removeEventListener('mousemove', mouseMoveEvent);
                }
            }
        } //end mouseMoveEvent
    } //end setSlideDragEvent
}

QuickSlide.prototype.arrowEvents = function(){
    if(this._arrowsActive){
        var self = this;
        self._arrowLeft.addEventListener("click", function(){
            self.shiftSlides('right');
        });
        self._arrowRight.addEventListener("click", function(){
            self.shiftSlides('left');
        });
         self._arrowLeft.addEventListener("touchstart", function(){
             self.shiftSlides('right');
        });
        self._arrowRight.addEventListener("touchstart", function(){
            self.shiftSlides('left');
        });
    }
}


QuickSlide.prototype.indicatorCycle = function(i){
    var self = this;
    var indicatorIndex = i;
    var slideIndex = self._activeSlide;
    var storeTransition = self.transition;
    var animationOffset = 50;
    self.transition = 80;
    self.setTiming();

    if(indicatorIndex > slideIndex){
        while(indicatorIndex > slideIndex){
            setTimeout(function(){
                self.shiftSlides('left');
            }, animationOffset);
            slideIndex++;
            animationOffset += 200;
        }
    }else if(indicatorIndex < slideIndex) {
        while(indicatorIndex < slideIndex){
            setTimeout(function(){
                self.shiftSlides('right');
            }, animationOffset);
            slideIndex--;
            animationOffset += 200;
        }
    }else {
        return;
    }
    setTimeout(function(){
        self.transition = storeTransition;
        self.setTiming();
    }, animationOffset + 50);
}

QuickSlide.prototype.indicatorEvents = function(){
    var self = this;
    var i = 0;
    for(i; i < this._indicatorItems.length; i++){
        setIndicatorEvent(i);
    }
    function setIndicatorEvent(i){
        var fired = true;
        self._indicatorItems[i].addEventListener('click', function(e){
            e.preventDefault(); //prevent touch events
            self.indicatorCycle(i);      
        });
        self._indicatorItems[i].addEventListener('touchstart', function(e){
             e.preventDefault(); //prevent click events
            self.indicatorCycle(i);
        });
    }
}

QuickSlide.prototype.throttledResize = function (cb) {
    var self = this;   
    var setTime;
    var timer = -101;
    var endCheck;
    var firstCycle = true;
    window.addEventListener('resize', function (e) {
        //throttlesresize event to interact with the dom once every 100ms
        if(timer < -100){
            setTime = new Date().getTime();
            timer = 0;
            self.setSliderHeight();
        }else {
            timer = setTime - new Date().getTime();
        }
        // on the first event set interval to watch for resize events to stop
        if (firstCycle){
            firstCycle = false;
            endCheck = setInterval(function () {
                if ((setTime - new Date().getTime()) < -150) {
                    clearInterval(endCheck);
                    firstCycle = true;
                }
            }, 100)
        } 
    });
}

QuickSlide.prototype.swipeRight = function (element, cb) {
    var element = document.querySelectorAll(element);
    var i = 0;
    for (i; i < element.length; i++) {
        this.swipe(element[i], 'right', cb);
    };
}

QuickSlide.prototype.swipeLeft = function (element, cb) {
    var element = document.querySelectorAll(element);
    var i = 0;
    for (i; i < element.length; i++) {
        this.swipe(element[i], 'left', cb);
    };
}

QuickSlide.prototype.swipe = function (el, direction, cb) {
    var self = this;
    var element = el;
    var body = document.querySelector('body');
    var swipebool;
    var startP;
    var startX;
    var startY;
    var directionModifier;
    switch (direction) {
        case 'right':
            directionModifier = -1;
            break;
        case 'left':
            directionModifier = 1;
            break;
        default: directionModifier = 1;;
    }
    element.addEventListener("touchstart", function (e) {
        swipebool = true;
        startX = e.changedTouches[0].clientX;
        startY = e.changedTouches[0].clientY;
    }, false);
    element.addEventListener("touchmove", function (e) {
        var distanceX = startX - e.changedTouches[0].clientX;
        var distanceY = startY - e.changedTouches[0].clientY;
        if (distanceY < 0) { distanceY *= -1; }
        if (distanceX <= 150 && swipebool && distanceY < (distanceX * directionModifier)) {
            e.preventDefault();//stops verical scrolling
            self.addClass(body, 'swiping');
            swipebool = false;
            return cb.call(this, el); 
        }
    }, false);
    element.addEventListener("touchend", function (e) {
        self.removeClass(body, 'swiping'); 
    }, false);
}

QuickSlide.prototype.hasClass = function (el, className) {
    if (el.classList) {
        return el.classList.contains(className)
    } else {
        return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
    }
}

QuickSlide.prototype.addClass = function (el, className) {
    if (el.classList) {
        el.classList.add(className)
    } else if (!this.hasClass(el, className)) {
        el.className += " " + className
    }
}

QuickSlide.prototype.removeClass = function (el, className) {
    if (el.classList) {
        el.classList.remove(className);
    } else if (this.hasClass(el, className)) {
        var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
        el.className = el.className.replace(reg, ' ');
    }
}

