# QuickSlide #

QuickSlide is a minimalistic slider written in vanilla javascript that instantiates quickly and is fully responsive. 

### setup ###

* add quickslide.js and quickslide.css to your project.
* use sample.html to copy the slider structure and include it into your project.
* the height of the slider is based on the height of your first slide and can be changed on screen resize using media queries.

### options ###

* transition - used to set transition time (default: 300)
* activeSlideCheck - if true tells the slider to loop through the slides looking for a active class. The slider then uses that slide as the active slide.(default: true)
* arrows - if true the slider is instantiated with next and previous arrows.(default: true)
* indicators - if true adds a slide indicator list beneath the slider.(default: true)


### sample with options ###
var slider = new QuickSlide('#slider' , 
    {
        transition: 400,
        activeSlideCheck: false,
        arrows: false,
        indicators: false
    }); 
